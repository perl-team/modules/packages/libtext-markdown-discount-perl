libtext-markdown-discount-perl (0.18-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.18.
  * Update debian/upstream/metadata.
  * Rewrite use-system-markdown.patch.
  * Update build dependencies.
  * Update file path in debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 02 Feb 2025 02:56:23 +0100

libtext-markdown-discount-perl (0.17-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.17.
  * Refresh use-system-markdown.patch.
  * Declare compliance with Debian Policy 4.7.0.
  * Update paths in debian/libtext-markdown-discount-perl.docs.
  * Update paths in debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 12 Jan 2025 02:38:23 +0100

libtext-markdown-discount-perl (0.16-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.16.
  * Update path in debian/libtext-markdown-discount-perl.docs.
  * Update third-party copyrights and licenses.
  * Make build dependency on libmarkdown2-dev versioned to match the
    included source version.
  * Refresh use-system-markdown.patch (offset).
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Wed, 04 Jan 2023 18:53:40 +0100

libtext-markdown-discount-perl (0.13-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.
  * Apply multi-arch hints. + libtext-markdown-discount-perl: Drop Multi-Arch: same.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 21:08:15 +0000

libtext-markdown-discount-perl (0.13-1) unstable; urgency=medium

  * Import upstream version 0.13.
  * Declare compliance with Policy 4.5.1
  * Install upstream README
  * Bump debhelper compatibility level to 13
  * Refresh patch

 -- intrigeri <intrigeri@debian.org>  Wed, 18 Aug 2021 18:01:33 +0000

libtext-markdown-discount-perl (0.12-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.12.
  * Add debian/upstream/metadata.
  * Refresh use-system-markdown.patch.
  * Declare compliance with Debian Policy 4.5.0.
  * Drop unneeded version constraints from (build) dependencies.
  * Update Build-Depends for cross builds.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 25 Jan 2020 18:57:26 +0100

libtext-markdown-discount-perl (0.11-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ intrigeri ]
  * Declare compliance with Debian Policy 4.2.1.
  * Mark as Multi-Arch: same.

 -- intrigeri <intrigeri@debian.org>  Mon, 29 Oct 2018 09:45:10 +0000

libtext-markdown-discount-perl (0.11-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ intrigeri ]
  * Declare compliance with Standards-Version 4.1.3.
  * Bump debhelper compatibility level to 11.
  * Enable all hardening build options.
  * Declare Rules-Requires-Root: no.
  * Enable the autopkgtest-pkg-perl test suite.
  * Add myself to Uploaders since a well-functioning team is not good
    enough for some reason.

 -- intrigeri <intrigeri@debian.org>  Sun, 21 Jan 2018 18:17:01 +0000

libtext-markdown-discount-perl (0.11-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Make build dependency on libmarkdown2-dev versioned.
  * Use debhelper 9.20120312 to get all hardening flags.
  * Update license text for discount/*.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Dec 2013 23:50:01 +0100

libtext-markdown-discount-perl (0.10-1) unstable; urgency=low

  * New upstream release
  * Bump upstream copyright years

 -- Alessandro Ghedini <ghedo@debian.org>  Sun, 25 Aug 2013 12:07:02 +0200

libtext-markdown-discount-perl (0.06-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Alessandro Ghedini ]
  * New upstream release
  * Update copyright and license for discount

 -- Alessandro Ghedini <ghedo@debian.org>  Fri, 26 Jul 2013 19:17:08 +0200

libtext-markdown-discount-perl (0.04-1) unstable; urgency=low

  * New upstream release
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Drop do-not-use-cstring.patch (merged upstream)
  * Bump debhelper compat level to 9
  * Update copyright to Copyright-Format 1.0
  * Bump Standards-Version to 3.9.4 (no changes needed)

 -- Alessandro Ghedini <ghedo@debian.org>  Sun, 23 Sep 2012 19:17:03 +0200

libtext-markdown-discount-perl (0.02-1) unstable; urgency=low

  * Initial release (Closes: #654383)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Tue, 03 Jan 2012 15:43:12 +0100
